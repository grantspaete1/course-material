<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Lecture 0.2 - Documentation

This module will introduce the following topics:

- [ ] The importance of documentation

- [ ] Techniques for effective documentation

- [ ] Introduction of documentation languages

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
