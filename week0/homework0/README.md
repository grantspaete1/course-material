<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Homework 0

## [Homework 0.1 - CATME **(16 Jan 2022 @ 23:59)**](https://purdue.brightspace.com/d2l/le/content/455265/viewContent/8620577/View)

This course will make use of [CATME](https://info.catme.org), a powerful team building software that has been designed by Purdue through a National Science Foundation Grant. This software is designed to create teams with the most effective skill sets as well as remove and biases often found in team building.

You should receive a link via email to register and complete the CATME survey. If you do not receive this email, please alert me asap.

## [Homework 0.2 - Syllabus [Perusall] (16 Jan 2022 @ 23:59)](https://purdue.brightspace.com/d2l/le/content/455265/viewContent/8620791/View)

Please read through the syllabus and make notes as you read. Please note items that either do or do not make sense, items that need improvement, etc. Essentially, this assignment accomplishes two things: 1) it requires you to read the syllabus and 2) it's free editing for me. :)

*This is a trial run of Perusall - there may be bugs!

## [Homework 0.3 - Documentation Discussion (13 Jan 2022 @ 08:00)](https://purdue.brightspace.com/d2l/le/content/455265/viewContent/8661789/View)

Think about some of the best examples of documentation that you have encountered. What makes them stand out? What do you think makes for good documentation?

Drop a note; share a link.

## [Homework 0.4 - File Naming Conventions [Perusall] (16 Jan 2022 @ 23:59)](https://purdue.brightspace.com/d2l/le/content/455265/viewContent/8673334/View)

Very quick read on file naming conventions from Harvard's Research Data Management. Make a note on anything new that you learn or anything you will apply to your file conventions in class.

## [Homework 0.5 - Markdown Tutorial (16 Jan 2022 @ 23:59)](https://gitlab.com/at21900/course-material/-/blob/main/module0/homework0/README.md)

Markdown is a documentation language used by scientists, researchers, programmers, [redditors](https://www.markdownguide.org/tools/reddit/), bloggers, etc. It's very simple, lightweight, but powerful for creating documentation on the web. Markdown directly builds to HTML (the language that webpages are written in) without the complex tagging requirements. 

**Assignment:**

- Please complete this [online tutorial](https://www.markdowntutorial.com/) (_10 minutes_)

- After completing the tutorial, please download this [README.md](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week0/homework0/README.md) document.
  ![download](https://gitlab.com/at21900/course-material/-/raw/main/img/homework0_download.png)

- Open the file with either Notepad on Windows or TextEdit on MacOS.

- Review the Markdown formatting I use.

- Using Markdown Guide's [Basic](https://www.markdownguide.org/basic-syntax/) and [Extended](https://www.markdownguide.org/extended-syntax/) Syntax as well as [GitLab Flavored Markdown](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/markdown.md) Syntax, add the following to this document:
  
  - A task list showing your class schedule.
  
  - A four column, three row table with the following: First Name, Last Name, Student ID, Email for three fake students.
  
  - Put the following code in a code block:
    
    // the loop function runs over and over again forever
    void loop() {
      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);                       // wait for a second
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      delay(1000);                       // wait for a second
    }
  
  - One feature of your choosing.

- Save your file as `at219_homework0.5_FirstLast_date.md`

- You may preview your file using [Editor.md](https://pandao.github.io/editor.md/en.html), but know that any GitLab Flavored Syntax will not compile here as it only works on GitLab. I will still be able to see it though.

- Upload to [Brightspace](https://purdue.brightspace.com/d2l/le/content/455265/viewContent/8674426/View).

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
