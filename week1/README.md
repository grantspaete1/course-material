<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# Week 1 - Introduction to Git and GitLab

This module will introduce the following topics:

- [ ] Git - a version control system

- [ ] GitLab - a remote git repository

As an overview, this module will include:

- [ ] Lecture 1.1 and 1.2 presentation, videos, and notes

- [ ] Lab 1 Assignment

- [ ] Homework 1 Assignments:
  
  - [ ] TBD
    
    

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
