# Homework 1

The homework this week is to complete [Lab 1](https://gitlab.com/purdue-uas/at21900/course-material/-/blob/main/week1/lab1/README.md). It is not necessarily a difficult assignment, but it does require a bit of focus and attention. Additionally, there will be an assigned reading available in Perusall on Thursday.

[![CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
