# Lab 1 - Git, GitLab, and Wiki Setup

[[_TOC_]]

## Introduction

The objectives of this lab are:

1. Review who uses Git
2. Install Git
3. Clone PX4 source code
4. Connect to GitLab and create a private repository for assignment submissions
5. Upload `at219_lab1_FirstLast_YYYYMMDD.md` in private repository
6. Write About Page for team Wiki
7. [Optional] Generate SSH keys for passwordless login.
8. [Optional] Continue soldering lab (as time allows)

Git is an industry standard distributed version control system used by nearly 10,000 companies including Netflix, reddit, and Lyft. Most importantly, git is used heavily in UAS programming and design. Open source UAS projects require the work of engineers and designers from all over the world working together on a single project. Git makes this possible by handling the version control for every individual's edits preventing editing collisions, irreversible code releases, or worse of all total deletion of the code base. 

---

## Part 0: Who's using git?

### Drone Code using Git

Take a few moments to follow the below links for some of the largest open source, collaborative UAS projects using git:

1. [PX4 Drone Autopilot](https://github.com/PX4/PX4-Autopilot)
2. [PX4 Drone Autopilot Documentation/Wiki](https://docs.px4.io/master/en/development/development.html)
3. [QGroundControl Ground Control Station](https://github.com/mavlink/qgroundcontrol)
4. [ArduPilot Autopilot](https://github.com/ArduPilot/ardupilot)
5. [Pixhawk Open Hardware](https://github.com/pixhawk)

### Assignment

Create a folder titled `lab1_FirstLast`. Within that folder create another folder titled `img` and a markdown file titled `at219_lab1_FirstLast_YYYYMMDD.md`. Answer the following questions in the markdown file that you just created.

1. Include your name at the top of the file.
2. What is covered in the README.md for these projects?
3. How many contributors does the ArduPilot Autopilot project have?
4. What trends do you notice between projects?
5. Does the Pixhawk repository contain code?

---

## Part 1: Downloading git

Git is the software that runs on your computer to take care of versioning. It tracks **local** file changes and in [Part]() we will see that it can sync those changes with a remote repository like GitLab.

Follow the instructions below for your correct operating system, [Mac](#mac) or [Windows](#windows)

#### Mac

1. Download and install [iTerm2](https://iterm2.com). iTerm is a powerful command line interface alternative to the stock Terminal program. A [command line interface](https://en.wikipedia.org/wiki/Command-line_interface) simply processes commands directly to the computer (through text instead of a graphical user interface (GUI))

2. Open iTerm

3. Copy and paste the following line into the terminal to install [zsh](https://ohmyz.sh)

```shell
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

4. Quit and restart iTerm

5. Copy and paste the following line into the terminal to check if git is already installed

```git
git --version
```

![lab1_gitversion_mac](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_gitversion_mac.png)

6. Git should already be installed in the MacOS system. The version doesn't matter for now. If git is not installed, simply download it from [here](https://sourceforge.net/projects/git-osx-installer/files/) and repeat step 5.

#### Windows

1. Download and install [PowerShell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2) MSI. PowerShell is a powerful command line interface alternative to the stock Command Prompt (CMD) program. A [command line interface](https://en.wikipedia.org/wiki/Command-line_interface) simply processes commands directly to the computer (through text instead of a graphical user interface (GUI))

2. Download git for Windows from [here](https://gitforwindows.org)

3. Open PowerShell and copy/paste the following line into the shell to check if git is already installed

```git
git --version
```

![lab1_gitversion_windows](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_gitversion_win.png)

### Assignment

Attach a screenshot of your git version to your markdown assignment file.

> :point_up: **Remember:** Images can be inserted using `![imageDescription](img/pictureFileName.png)`.

:clap: **[Congradulations](https://www.youtube.com/watch?v=SC4xMk98Pdc)** You now have git installed! 

## Part 2: Cloning PX4 Source Code

Any code that is publically available on a remote repository, like GitLab or GitHub can be cloned directly to your computer's harddrive. From there, you can make changes to the code to meet your needs. If you think your changes might be useful to the project, then you can even merge your work back into the main project.

Follow these instructions to `clone` the PX4 source code. Keep in mind, the PX4 source code is *everything* needed to fly any and all of [these](https://docs.px4.io/master/en/airframes/airframe_reference.html) airframes!

1. Open iTerm/PowerShell. Referred to as terminal from here on.

2. Terminal opens to your computer's root director. We typically do not work with files at the root, so let's navigate to our Documents folder using the `cd` command which stands for *change directory*.

3. After typing in `cd ` (with a space) you may initiate autocomplete by pressing the *tab* key until the desired folder is highlighted. Then press *enter* to select the folder. Repeat the *tab/enter* sequence until you've completed the path to your documents folder.

![lab1_part2_cd](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part2_cd.gif)

4. Now, we are ready to clone the repository, but first we need the link to the remote repository.
   
   1. Open the GitHub page for PX4, [here](https://github.com/PX4/PX4-Autopilot)
   
   2. Select the green *Code* dropdown box.
   
   3. Copy the https link.

```git
https://github.com/PX4/PX4-Autopilot.git
```

![lab1_part3_copylink](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_copylink.gif)

5. Using the `git clone` command, we can copy the PX4 source code into out downloads folder using the following command.

```git
git clone https://github.com/PX4/PX4-Autopilot.git
```

![lab1_part3_clone](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_clone.gif)

:clap: **[Congratulations](https://www.youtube.com/watch?v=SC4xMk98Pdc)** You now have all of the source code for the PX4 Autopilot in your documents folder!

### Assignment

Read the `CONTRIBUTING.md` document to see how to contribute your work back to the project. Summarize the procedure and define (using Google) what a fork is in your `at219_lab1_FirstLast_YYYYMMDD.md` file.

## Part 3: Connecting to GitLab

So far we worked with [git](#git) locally and cloned code from a [remote repository](#remote-repository). Now we will set up our own private repository. This repository will be used to submit assignments as well as a sandbox for getting used to [git](#git). 

First, let's initially set up our local git to work with GitLab. Issue the following commands in terminal. The top command shows generic text, and the bottom command shows a usage example. Replace with your information accordingly:

1. Add your username

```git
git config --global user.name "your_username"
```

```git
git config --global user.name "Drone_Prof"
```

2. Add your email address

```git
git config --global user.email "your_email_address@example.com"
```

```git
git config --global user.email "rose196@purdue.edu"
```

3. Check the configuration

```git
git config --global --list
```

```git
git config --global user.name "Drone_Prof"
```

4. The `--global` option tells Git to always use this information so that we do not have to enter it again.

Now, let's create a repository for individual submissions

1. Log into [GitLab](https://gitlab.com/users/sign_in)

2. The homepage shows the projects that you are a part of. Yours is empty for now.

3. Create a new project
   
   1. Click the blue *New Project* button
   
   2. Choose *Create blank project*
   
   3. Project name: AT219 FirstName LastName
   
   4. Click the blue *Create project* button.
   
   ![lab1_part3_newproj](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_newproj.gif)

4. Clone new project to computer
   
   1. Create a folder titled `AT219` for this course in your documents folder.
   
   2. `cd` into `AT219` folder.
   
   ![lab1_part3_cdat219](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_cdat219.gif)
   
   3. Copy the *Clone with HTTPS* link.
   
   ![lab1_part3_clonelink](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_clonelink.gif)
   
   4. Back in terminal, paste the following `git clone` command, but replace my https link with your own. Press enter and the files (`README.md`) will be downloaded to your computer. :sunglasses: 
   
   ```git
   git clone https://gitlab.com/Drone_Prof/at219-nathan-rose.git
   ```
   
   5. You will be required to enter your GitLab username and password at the prompts. \*
   
   ![lab1_part3_clonecommand](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_clonecommand.gif)

\* Troubleshooting: if you receive a `http basic: access denied` error, enter the following (this is an elevated privilege command requiring your administrative password):

```git
sudo git config --system --unset credential.helper
```

:tada: Perfect! Now you've cloned your first private repo.

## Part 4: Add assignment to GitLab

Now that we have our remote repo cloned to our AT219 folder, we have two copies of the same thing, one on GitLab and one on our local machine. Editing files happens on the local machine which can then be pushed up to a remote repo. Let's try four new git commands, `status`, `add`, `commit`, and `push`

In [Homework 0.5](https://gitlab.com/purdue-uas/at21900/course-material/-/tree/main/week0/homework0#homework-05-markdown-tutorial-16-jan-2022-2359), we wrote Markdown code in a simple text editor like Notepad or TextEdit. Now that you have an understanding of the code behind the Markdown, you may use a WYSIWYG (what you see is what you get) editor. I recommend [Mark Text](https://marktext.app), a simple Markdown editor. There will be no tutorial for Mark Text, but questions on how to use it are welcome.

![lab1_marktext](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part4_marktext.png) 

### `git status`

This command it used to check the status of your repo as your make changes. 

1. Open Terminal. Be sure that you are in the correct directory/folder, if not follow the `cd` command steps in [Part 2](#part-2-cloning-px4-source-code) until you enter the `at219-first-last` folder (one level beyond the `AT219` folder). Enter the following to check the status. 

```git
git status
```

![lab1_part3_status1command](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_status1command.gif)

2. You should see the following since we have not made any changes to the cloned files or structure within the `at219-first-last folder`.

```git
On branch main
Your branch is up to date with 'origin/main'.

nothing to commit, working tree clean
```

3. Open `README.md` from the repo we cloned in [Part 3](#part-3-connecting-to-gitlab) using either a text editor or Mark Text.

4. Add the following line to the bottom of the `README.md` file and save.

```markdown
hello world!
```

3. run `git status` again.

### Assignment

Copy the text output from the above step into your `at219_lab1_FirstLast_YYYYMMDD.md` file.

### `git add`

Now that we have made a change to the `README.md` file, we need to *stage* the file. This gets it ready to become part of the Git snapshot.

1. Run the following

```git
git add .
```

2. The period simply tells git that you want to *stage* all of the files that have been modified in the local Git repo. Alternatively, this could be done on individual files, like `git add README.md`.

### `git commit`

The next step is to commit our staged files. This tells our local Git database to take a snapshot of this version of the file (this snapshot is stored for future use should we ever need to rollback changes) 

1. Issue the following command

```git
git commit -m "commit_message"
```

2. It is important to supply a useful commit message to keep track of the changes that you have made in plain english.

### `git push`

We have staged our changes, committed them locally, and now it is time to push them to the remote repo for out collaborators.

1. Issue the following command

```git
git push
```

2. You will see that the changes are pushed up to GitLab.

3. Verify that the changes appear on GitLab (look at the **Last Update** column)

![lab1_part3_addcommitpushcommands](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part3_addcommitpushcommands.gif)

### Assignment

1. Copy the `at219_lab1_FirstLast_YYYYMMDD.md` file into the `at219-first-last` folder.

2. Run the commands necessary to put the file into your private GitLab Repo. Hint: you can verify this by logging into GitLab and looking at your files.

3. Add instructor to private repo.
   
   1. Log into GitLab
   
   2. Click on the `AT219 First Last` project.
   
   3. In the left sidebar, hover over *Project Information*
   
   4. Choose *Members*
   
   5. Add `Drone_Prof` as a *Maintainer* role.
   
   6. I will get an email when completed.

## Part 5: Team Wiki

:muscle: Here we are, at the culmination of the first two weeks of Documentation, Git, and GitLab! Now it is time to begin the Team Wiki page. Teams have already been created and you have been added to a team over the weekend. Working on one computer, work with your team mates to create an *About* page describing your team.

### Create New Team Project

1. Open Your Groups: *GitLab Menu > Groups > Your Groups*

![lab1_part5_groups](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part5_groups.png)

2. From Groups open *Purdue UAS > AT21900 > "GroupName"*

3. Create New Project just as you did in [Part 3](#part-3-connecting-to-gitlab). Name it `AT219 Wiki`

4. Click *Wiki* in the left sidebar.

![lab1_part5_wikitab](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part5_wikitab.png)

5. Click *Create your first page*.

![lab1_part5_createpage](https://gitlab.com/purdue-uas/at21900/course-material/-/raw/main/img/lab1_part5_createpage.png)

6. Select *Clone Repository* from the right sidebar.

7. Copy the HTTPS address.

8. Clone the repo just as we did in [Part 2](#part-2-cloning-px4-source-code) into your `AT219` folder. Note: if you are in your at219-first-last folder/local repo, you can use `cd ..` to move back a file level. Do not clone a repo into another repo. 🤯

9. Add and edit `.md` files. When these are pushed back to the remote repo, they will become pages in the Wiki.

10. For now, only one person should edit at a time. After a team member performs a `push`, the other team members should perform a `pull` to get the latest edits.

11. We can solve any issues that arise with some Googling and/or reviewing the [resources](#resources).

## [Optional] Generate SSH Keys

Typing in your email and password really becomes a hassle after a while. Follow the instructions in the GitLab documentation to generate and upload ssh keys - your keyboard will thank you!

1. [Generate a SSH key pair](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair)

2. [Add an SSH key to your GitLab account](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account)

3. [Verify that you can connect](https://docs.gitlab.com/ee/ssh/#verify-that-you-can-connect)

That's it!

## Resources

1. [GitLab Documentation](https://docs.gitlab.com)

2. [Pro Git Book](https://git-scm.com/book/en/v2), especially [Section 2.4 Git Basics - Undoing Things]

3. [Atlassian Tutorials](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)

4. [Interactive Git Cheatsheet](https://ndpsoftware.com/git-cheatsheet.html#loc=index;)

[![CC BYNCSA 40](https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
