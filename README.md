<img title="" src="https://gitlab.com/at21900/course-material/-/raw/main/img/purdue_logo.png" alt="Purdue SATT Logo" data-align="center">

# AT 21900 Unmanned Aerial Systems (UAS) Design, Build, Test

Lecture (NISW 184)
08:30-09:20 [T/TH]

Lab (COMP 101)
09:30-11:20 [T] or 13:30-15:20 [F]

[![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg

"AT 21900 Unmanned Aerial Systems (UAS) Design, Build, Test Course" by [Nathan Rose, Purdue University](https://gitlab.com/at21900/course-material) is licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-nc-sa/4.0/)

**Course in Progress:** this course is currently being developed! Updates will be made available on this GitLab repository and via the [Brightspace](https://purdue.brightspace.com/d2l/login) course page.

# Syllabus

[TOC]

## Course Information

- **AT 21900 Unmanned Aerial Systems Design, Build, Test**

- **CRN:** 14343 (Lecture), 14327 (Lab [T]), 14294 (Lab [F])

- **Meeting day(s) and time(s)**: 
  
  - Lecture (NISW 184): 08:30-09:20 [T/TH]
  
  - Lab (COMP 101): 09:30-11:20 [T] or 13:30-15:20 [F]
  
  - Open Lab (COMP 101): TBD

- **Instructional Modality:** Face-to-Face (Lecture/Lab)

- **Course credit hours:** 3

- **Prerequisites**: 
  
  - AT 20900
  
  - FAA Part 107 Certificate

## Instructor Contact Information

- **Name of the instructor:** Nathan Rose

- **Office Location:** NISW 118A

- **Office Phone Number:** [765.496.0933](tel:+17654960933)

- **Purdue Email Address:** [nathanrose@purdue.edu](mailto:%20nathanrose@purdue.edu?subject=[AT21900_S22])

- **Student consultation hours, times, and location (in order of preference/response time)****:**
1. [Brightspace](https://purdue.brightspace.com) Discussion Board

2. Group Office Hours: [13:00-14:00 [T]](https://teams.microsoft.com/l/meetup-join/19:meeting_ZTc4YzJmYWEtNzg5OC00Mzg3LTgxY2EtNjY3MDcyMzc0MjY2@thread.v2/0?context=%7B%22Tid%22:%224130bd39-7c53-419c-b1e5-8758d6d63f21%22,%22Oid%22:%225e9c844a-d0ba-4e2a-9b8c-62de0ea40f71%22%7D) and [09:30-10:00 [F]](https://teams.microsoft.com/l/meetup-join/19:meeting_NjA4NGIwMDMtNGRmNi00Y2ExLWJjZTAtZWY3OWRmMGM1Y2Ew@thread.v2/0?context=%7B%22Tid%22:%224130bd39-7c53-419c-b1e5-8758d6d63f21%22,%22Oid%22:%225e9c844a-d0ba-4e2a-9b8c-62de0ea40f71%22%7D) (NISW 118A and Virtual via Teams Link)

3. Individual Office Hours: As “chat hours” available via [scheduler](http://eng.purdue.edu/jump/2f6526b).

4. All others: Students may email specific request via this [link](mailto:%20nathanrose@purdue.edu?subject=[AT21900_S22]) (subject line “[AT21900_S22]”)

## Course Description

This course will further develop skills and understanding of small aerial systems. Emphasis will be on design, selection, construction, flight test, evaluation, and repair, of stabilized unmanned aircraft. Emphasis will be placed on advancing manual flight control skills utilizing an advanced simulator and student-constructed rotorcraft. Additionally, students will design and deploy scenario-specific sensors using student-constructed rotorcraft. Students will be expected to demonstrate their sensor payload not only through lab and field demonstration, but also through documentation.

## Learning Resources, Technology & Texts

Required technology and readings:

- Required readings will be made available via Brightspace/Perusall

- [Arduino IDE](https://www.arduino.cc/en/software)

- [Mission Planner](https://ardupilot.org/planner/docs/mission-planner-installation.html)) OR [QGroundControl](https://docs.qgroundcontrol.com/master/en/getting_started/download_and_install.html)

- [Microsoft Office Suite](https://www.itap.purdue.edu/services/microsoft-office-365.html)

## Learning Outcomes and Teaching Philosophy

Broadly, students will be able to design, build, and pilot unmanned aerial systems with customizable payloads. Specifically, students will be able to:

1. Apply flight principles to building a UAS capable of performing a specific operation.

2. Analyze and compare flight characteristics of UAS under different environmental and physical conditions.

3. Predict and compare flight characteristics of UAS using different components and configurations.

4. Integrate custom sensor payload into UAS design.

5. Safely and efficiently coordinate automated flights with team members.

6. Develop an enterprise-ready online user manual for UAS and sensor payload.

Additionally, to
succeed in this course, and in industry, students will be expected to
perform and document all building, repairing, and maintenance
according to industry standards. Finally, students will be working on
teams – these are your colleagues and should be respected as such.

UAS is a rapidly evolving field. This means that I will not have all the latest and greatest information on new and upcoming technology. I strongly encourage students to share new technology, new UAS use cases, and ideas about the future of UAS. I am also a learner and am eager to learn *with* my students through class discussion and feedback.

Finally, as an instructor of technology, I take a similar oath to medical doctors: “First do no harm.” As we have witnessed time and time again, technology can and will be used for nefarious intentions. It is my goal to teach my students to look beyond the technology to the people that will use it and to the people that it may be used on. It is our collective responsibility to ensure that our technology is designed without biases.

## Assignments

All assignments will be posted on [Brightspace](https://purdue.brightspace.com/) along with their due dates. Both lecture and lab will use the “Spring 2022 AT 21900 003 LEC” tab. Students are encouraged to [subscribe to the course calendar](https://documentation.brightspace.com/EN/le/calendar/learner/subscribe_to_calendar.htm).

**Assignment Overview:**

**CATME Team Generator (Pass/Fail)**: Students will complete a strengths analysis survey through Purdue’s CATME program during the first week. This survey will automatically pair students based on strengths, skills, and availability. This assignment will be counted towards the quiz grade.

**Quizzes**: Assigned periodically in Brightspace and automatically graded in Brightspace.

**Flight Logbook (Pass/Fail)**: Students will log all flight hours in a physical or digital logbook. Students will be required to complete 2 hours of flight time on the opensource UAS and 2 hours on the DJI Mavic Pro 2. All flights are to be completed in pairs at approved flight areas while wearing the fashion award-winning Purdue UAS vests. Students will prove flight hours via visual observer signature AND flight software/app screenshot showing logged hours.

**Readings and Discussions**: Students will be assigned periodic readings through [Brightspace](https://purdue.brightspace.com/) using the [Perusall](https://perusall.com/) plug in. These readings will be used for in-class discussions and graded based on participation and online notations.

**Project Demonstration**: Students will be required to demonstrate the performance of their UAS to perform the assigned scenario specific task.

**Team GitLab & Wiki**: This online git repository and wiki will document team progress including, but not limited to: assembly/maintenance logs, source code, UAS performance evaluations, flight plans, and end-user manual.

**CATME Team Evaluation**: Students will be required to evaluate the strengths of themselves and their team members in a CATME end of semester survey. The link to their survey will be automatically generated and sent to students at the end of the semester.

**Course Feedback**: Students will provide course feedback to gauge and guide instructor performance and direction. Feedback will be collected as “exit tickets” including notes about what worked, what could use improvement, and further topics of interest.

**Late Work Policy:**

No late work will be accepted. If the student has an absence due to illness with doctor’s slip, military duties, family emergency or other issues accepted by the Dean of Students, the student is responsible for providing proof to the Dean of Students. Appropriate accommodations will be made in these circumstances.

## Grading Scale

The student’s grade will be determined in three ways: 1) by their individual performance on quizzes, readings, discussions, and teamwork, 2) by their team’s performance on the semester project, and 3) by the semester end CATME evaluations including both self-assessment and team-assessment.

The grades will be weighted and evaluated as follows:

| **Assignment(s)**                                                                          | **Percentage of Grade (100%)** | **Notes**                                                                                                                                                                      |
| ------------------------------------------------------------------------------------------ | ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| CATME Team Generator (Pass/Fail)<br/>Quizzes<br/>Course Feedback<br/>CATME Team Evaluation | 25%                            | Each of these assignments counts towards the overall quiz grade. Additional assignments may replace quiz items and will be noted clearly in class and updated on the syllabus. |
| Readings and Discussion                                                                    | 15%                            | Readings and discussion grades come from participation on Perusall (50%) and in class (50%).                                                                                   |
| Flight Logbook (Pass/Fail)                                                                 | 5%                             |                                                                                                                                                                                |
| Project Demonstration                                                                      | 20%                            |                                                                                                                                                                                |
| Team GitLab & Wiki                                                                         | 35%                            | Wiki will be graded periodically based on milestones. This will give students the opportunity to improve their wikis.                                                          |

## Academic Integrity

> “As a Boilermaker pursuing academic excellence, I pledge to be honest and true in all that I do. Accountable together—We Are Purdue.”

Academic integrity is the foundation of so much of the technology, philosophy, experiences, etc. we have come to know and love. We are pioneers of new solutions to many of today’s biggest problems – you are now an integral part of that. However, that great power requires great responsibility and that begins in the classroom. Students are expected to be familiar with [Purdue’s Academic Integrity website](https://www.purdue.edu/odos/osrr/academic-integrity/undergraduate.html) and will be required to [pledge](https://www.purdue.edu/odos/osrr/academic-integrity/undergraduate.html%23pledge) their work accordingly. This course relies heavily on teamwork and collaboration and this policy is not intended to limit the sharing and growth of knowledge. Students will be made aware of work that should be completed individually and work that should be completed collaboratively. Students should always seek to give credit to the appropriate team member, author, institution, etc. It is the student’s responsibility to ensure that their work does not violate Purdue’s Academic Integrity Policy. Questions about the policy should be emailed to the instructor before submission. Violations will result in a grade reduction determined by the severity of the infraction with guidance from the [Office of Student Rights and Responsibilities](https://www.purdue.edu/odos/osrr/index.html). Finally, students share the same responsibility as myself to report academic integrity violations (and may be held accountable for not reporting) through the [“Academic Dishonesty” incident reporting form](https://cm.maxient.com/reportingform.php?PurdueUniv&layout_id=10) or by emailing the [instructor and integrity@purdue.edu](mailto:integrity@purdue.edu,%20rose196@purdue.edu?subject=Academic%20Integrity%20Report%20-%20AT21900%20S22).

## Attendance Policy

This course relies on hands-on experiential learning, and therefore requires student
attendance. However, considering the ongoing pandemic, students have the additional responsibility to protect their fellow classmates and instructors in accordance with the [Protect Purdue Website](https://protect.purdue.edu/). For this reason, students will not be graded on their attendance, but instead on the quality of their work. Missing lecture and/or lab will require the student to make up work outside of regular class/lab time during instructor office hours and/or open lab hours, in accordance to the individual plan agreed upon by instructor and student. It is my goal to manage and mitigate barriers to learning – please email as soon as possible (preferably before missing) to start working on a plan to make up the work. More generally, and per the university policy/suggestion:

“The University expects that students will attend classes for which they are registered. At times, however, either anticipated or unanticipated absences can occur. The student bears the responsibility of informing the instructor in a timely fashion, when possible. The instructor bears the responsibility of trying to accommodate the student either by excusing the student or allowing the student to make up work, when possible. The University expects both students and their instructors to approach problems with class attendance in a manner that is reasonable.”

## Academic/Classroom Guidance Regarding Protect Purdue

Simply put, we have a collective responsibility to protect ourselves and our colleagues during the ongoing pandemic. Students are required to follow [Protect Purdue](https://protect.purdue.edu/) protocols and any student who has substantial reason to believe that another person is threatening the safety of others by not complying with [Protect Purdue](https://protect.purdue.edu/) protocols is encouraged to report the behavior to and discuss the next steps with their instructor. Students also have the option of reporting the behavior to the [Office of the Student Rights and Responsibilities](https://www.purdue.edu/odos/osrr/). See also [Purdue University Bill of Student Rights](https://catalog.purdueglobal.edu/policy-information/student-information-services/student-bill-rights/) and the Violent Behavior Policy under University Resources in Brightspace.

Therefore, students showing responsibility by quarantining/isolating in accordance with [Protect Purdue](https://protect.purdue.edu/) will be given equal opportunity and access to learning. Students should plan to communicate with instructor by [email](mailto:nathanrose@purdue.edu?subject=[AT21900_S22]) to propose and agree on a plan.

## Course Schedule

The course schedule will be provided through [Brightspace](https://purdue.brightspace.com/) as well as the course [GitLab](https://gitlab.com/at21900/course-material). Students are encouraged to [subscribe to the course calendar](https://documentation.brightspace.com/EN/le/calendar/learner/subscribe_to_calendar.htm) which will include assignment due dates as well as key dates from the [Purdue Academic Calendar](https://www.purdue.edu/registrar/calendars/2021-22-Academic-Calendar.html).

## Nondiscrimination Statement

Purdue University is committed to maintaining a community that recognizes and values the inherent worth and dignity of every person; fosters tolerance, sensitivity, understanding, and mutual respect among its members; and encourages each individual to strive to reach his or her potential. In pursuit of its goal of academic excellence, the University seeks to develop and nurture diversity. The University believes that diversity among its many members strengthens the institution, stimulates creativity, promotes the exchange of ideas, and enriches campus life. A hyperlink to Purdue’s full [Nondiscrimination Policy Statement](https://www.purdue.edu/purdue/ea_eou_statement.php) is included in our course Brightspace under University Policies.

## Accessibility

I am committed to making learning experiences accessible. If you anticipate or experience physical or academic barriers based on disability, you are welcome to let me know so that we can discuss options. You are also encouraged to contact the Disability Resource Center at: [drc@purdue.edu](mailto:drc@purdue.edu) or by phone: 765-494-1247.

## Mental Health/Wellness Statement

I welcome my students to be open with me about challenges they are facing with their mental health and wellness. I offer my office and my inbox as safe spaces for students to communicate openly with me. Below are some additional resources provided by Purdue University.

If you find yourself beginning to feel some stress, anxiety and/or feeling slightly overwhelmed, try [WellTrack](https://purdue.welltrack.com/). Sign in and find information and tools at your fingertips, available to you at any time. 
If you need support and information about options and resources, please contact or see the [Office of the Dean of Students](http://www.purdue.edu/odos). Call 765-494-1747. Hours of operation are M-F, 8 am- 5 pm.
If you find yourself struggling to find a healthy balance between academics, social life, stress, etc., sign up for free one-on-one virtual or in-person sessions with a [Purdue Wellness Coach at RecWell](https://www.purdue.edu/recwell/fitness-wellness/wellness/one-on-one-coaching/wellness-coaching.php). Student coaches can help you navigate through barriers and challenges toward your goals throughout the semester. Sign up is completely free and can be done on BoilerConnect. If you have any questions, please contact Purdue Wellness at [evans240@purdue.edu](mailto:evans240@purdue.edu).
If you’re struggling and need mental health services: Purdue University is committed to advancing the mental health and well-being of its students. If you or someone you know is feeling overwhelmed, depressed, and/or in need of mental health support, services are available. For help, such individuals should contact [Counseling and Psychological Services (CAPS)](https://www.purdue.edu/caps/) at 765-494-6995 during and after hours, on weekends and holidays, or by going to the CAPS office on the second floor of the Purdue University Student Health Center (PUSH) during business hours. 
CAPS also offers resources specific to COVID-19 on its [website](https://www.purdue.edu/caps/covid-19/index.html). Topics range from “Adjusting to the New Normal” to “How to Talk with Professors about Personal Matters.” 

## Basic Needs Security

Any student who faces challenges securing their food or housing and believes this may affect their performance in the course is urged to contact the Dean of Students for support. There is no appointment needed and Student Support Services is available to serve students 8 a.m.-5 p.m. Monday through Friday. Considering the significant disruptions caused by the current global crisis as it relates to COVID-19, students may submit requests for emergency assistance from the [Critical Need Fund]([Critical Need Fund / Disaster and Emergency Student Assistance Fund - Office of the Dean of Students - Purdue University](https://www.purdue.edu/odos/resources/critical-need-fund.html))

## Emergency Preparation

In the event of a major campus emergency, course requirements, deadlines and grading percentages are subject to changes that may be necessitated by a revised semester calendar or other circumstances beyond the instructor’s control. Relevant changes to this course will be posted onto the course website or can be obtained by contacting the instructors or TAs via email or phone. You are expected to read your @purdue.edu email on a frequent basis.
